import datetime
import csv

import requests 

api_key = "c1db3fa119584159"
lat_long = 37.776289,-122.395234

# initialise your csv file
outfile_path = '2013_14_demo_weather.csv'
writer = csv.writer(open(outfile_path, 'w'))

#headers = ['date','mean_temp','max_temp','min_temp','precipitation','mean_pressure','humidity','snowfall']
headers = ['date','mean_temp','max_temp']
writer.writerow(headers)

# enter the first and last day required here
start_date = datetime.date(2013,6,3)
end_date = datetime.date(2014,6,3)
#http://api.wunderground.com/api/c1db3fa119584159/history_20060405/q/37.776289,-122.395234.json
date = start_date
while date <= end_date:
    # format the date as YYYYMMDD
    date_string = date.strftime('%Y%m%d')
    # build the url
    url = ("http://api.wunderground.com/api/c1db3fa119584159/history_%s/q/%s.json" %
              (api_key,date_string,lat_long))
    # make the request and parse json
    data = requests.get(url).json()
    print(data)
    # build your row
# for history in data['history']['dailysummary']:
    # for history in data['history']['dailysummary']:
    #     row = []
    #     row.append(str(history['date']['pretty']))
    #     row.append(str(history['meantempi']))
    #     row.append(str(history['maxtempi']))
    #     row.append(str(history['mintempi']))
    #     row.append(str(history['precipi']))
    #     row.append(str(history['meanpressurei']))
    #     row.append(str(history['humidity'])) 
    #     row.append(str(history['snowfalli'])) 
    #     writer.writerow(row)
    # # increment the day by one

    for history in data['history']['observations']:
        row = []
        row.append(str(history['date']['pretty']))
        row.append(str(history['tempm']))
        row.append(str(history['wspdm']))       
        writer.writerow(row)
    date += datetime.timedelta(days=1)