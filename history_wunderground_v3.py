import requests
import csv
from dateutil.rrule import *
from dateutil.parser import *

# Variables
daySt = "20130603" # state date
dayEnd = "20140603" # end date
outPath = 'C:\wamp\www\Data_collction_oos\Weather_Data' # output path
lat_long = 37.776289,-122.395234
api = 'c1db3fa119584159' # developer API key

# Create list of dates between start and end
days = list(rrule(DAILY, dtstart=parse(daySt), until=parse(dayEnd)))

with open(outPath + lat_long + '_' + '2013_14' + '.csv', 'wb') as csvfile:
		f = csv.writer(csvfile)
		f.writerow(["date", "mean_temp", "max_temp", "min_temp", "precipitation", "mean_pressure", "humidity", "snowfall", "wgustm", "wgusti", "wdird", "wdire", "vism", "visi", "pressurem", "pressurei", "windchillm", "windchilli", "heatindexm", "heatindexi", "precipm", "precipi", "conds", "fog", "rain", "snow", "hail", "thunder", "tornado"])
#		'date','mean_temp','max_temp','min_temp','precipitation','mean_pressure','humidity','snowfall'
# Create daily url, fetch json file, write to disk
for day in days:
	r = requests.get('http://api.wunderground.com/api/' + api + '/history_' + day.strftime("%Y%m%d") + '/q/' + lat_long + '.json')
	data = r.json()['history']['dailysummary']
	print(data)
		for elem in data:
			f.writerow([elem["utcdate"]["year"] + elem["utcdate"]["mon"] + elem["utcdate"]["mday"] + 'T' + elem["utcdate"]["hour"] + elem["utcdate"]["min"], elem["meantempi"], elem["maxtempi"], elem["mintempi"], elem["precipi"], elem["meanpressurei"], elem["humidity"], elem["snowfalli"]])

			#     row.append(str(history['date']['pretty']))
    #     row.append(str(history['meantempi']))
    #     row.append(str(history['maxtempi']))
    #     row.append(str(history['mintempi']))
    #     row.append(str(history['precipi']))
    #     row.append(str(history['meanpressurei']))
    #     row.append(str(history['humidity'])) 
    #     row.append(str(history['snowfalli'])) 
